<?php

use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_todos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->constrained('categories')->cascadeOnDelete()->restrictOnUpdate();
            $table->foreignId('todo_id')->constrained('todos')->cascadeOnDelete()->restrictOnUpdate();
            $table->integer('position');
            $table->unique(['category_id', 'todo_id']);
            $table->unique(['category_id', 'position']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_todos');
    }
}
