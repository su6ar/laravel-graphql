<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryTodo;
use App\Models\Todo;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    private int $position;
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        $th = $this;
        Category::truncate();
        Category::factory()
            ->count(1)
            ->create();
        Category::factory()
            ->count(4)
            ->afterCreating(function (Category $category) use ($th) {
                $th->position = 0;
                Todo::factory()->count(random_int(1, 4))->afterCreating(function (Todo $todo) use ($category, $th) {
                    $categoryTodo = CategoryTodo::create(['category_id' => $category->id, 'todo_id' => $todo->id, 'position' => ++$th->position]);
                    $category->todos()->attach($todo);
                    $category->categoryTodos()->save($categoryTodo);
                })->create();
            })
            ->create();
    }
}
