<?php

namespace Tests\Unit;

use App\Models\User;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    public function testQueriesUsers(): void
    {
        $user = User::factory()->create();
        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                users {
                    data {
                        _id
                        email
                    }
                }
            }
        ')->assertJson([
            'data' => [
                'users' => [
                    [
                        'data' => [
                            '_id' => $user->_id,
                            'email' => $user->email,
                        ],
                    ],
                ],
            ],
        ]);
    }
}
