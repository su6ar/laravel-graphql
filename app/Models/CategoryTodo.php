<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\BelongsTo;


class CategoryTodo extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = ['_id', 'category_id', 'todo_id', 'position'];

    public function todo(): BelongsTo
    {
        return $this->belongsTo(Todo::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
