<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\BelongsToMany;
use Jenssegers\Mongodb\Relations\HasMany;

class Todo extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['_id', 'name', 'description'];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class)
            ->withPivot(['position'])
            ->using(CategoryTodo::class);
    }

    public function categoryTodos(): HasMany
    {
        return $this->hasMany(CategoryTodo::class, 'category_id', '_id');
    }
}
