<?php

namespace App\GraphQL\Mutations;

use App\Models\Category;
use App\Models\CategoryTodo;
use App\Models\Todo;

class CreateCategoryTodo
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args): CategoryTodo
    {
        $item = CategoryTodo::create(['position' => $args['position']]);
        $todo = Todo::find($args['todo']['connect']);
        $category = Category::find($args['category']['connect']);
        $item->todo()->associate($todo);
        $item->category()->associate($category);
        $item->loadMissing(['todo', 'category']);
        return $item;
    }
}
